package com.softwaresushi.scottify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mikepenz.materialdrawer.DrawerBuilder;

public class MainActivity extends AppCompatActivity
{
  private static final String      TAG                    = Constants.TAG + "main";
  public static final  String      ACTION_UPDATE_SEEK_BAR = "ACTION_UPDATE_SEEK_BAR";
  public static final  String      EXTRA_DURATION         = "duration";
  public static final  String      EXTRA_CURRENT_POSITION = "current-position";
  private              ImageView   mAlbumCoverImage       = null;
  private              TextView    mArtistAndAlbumText    = null;
  private              TextView    mSongText              = null;
  private              ImageButton mSkipPreviousButton    = null;
  private              ImageButton mPlayPauseButton       = null;
  private              ImageButton mSkipNextButton        = null;
  private              ImageButton mStopButton            = null;
  private              SeekBar     mSeekBar               = null;
  private              TextView    mSongTimeText          = null;

  private boolean mUserMovingSeekBar   = false;
  private long    mCurrentSongDuration = -1;

  // The number of individual steps in the seek bar.
  // More steps == smoother scrolling.
  private static final int SEEK_BAR_STEPS = 1500;

  public static final String ACTION_UPDATE_STATE           = "update-state";
  public static final String EXTRA_STATE                   = "state";
  public static final String EXTRA_ALBUM_DRAWABLE_RESOURCE = "album-drawable-resource";
  public static final String EXTRA_ALBUM_TITLE             = "album-title";
  public static final String EXTRA_SONG_TITLE              = "song-title";
  public static final String EXTRA_ARTIST_NAME             = "artist-name";

  public static final int STATE_PLAYING = 0;
  public static final int STATE_STOPPED = 1;
  public static final int STATE_PAUSED  = 2;

  boolean mIsPlaying = false;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    Intent intent = getIntent();
    if (intent != null && ACTION_UPDATE_STATE.equals(intent.getAction()))
    {
      // Activity created because of a UPDATE intent, probably not intended
      // so lets just close (finish) the Activity.
      finish();
    }
    else
    {
      setContentView(R.layout.activity_main);
      setupUi();

      new DrawerBuilder().withActivity(this).build();

      LocalBroadcastManager lbm    = LocalBroadcastManager.getInstance(this);
      IntentFilter          filter = new IntentFilter();
      filter.addAction(ACTION_UPDATE_SEEK_BAR);

      Log.d(TAG, "Registering LocalBroadcastReceiver");
      lbm.registerReceiver(new LocalBroadcastReceiver(), filter);

    }
  }

  private class LocalBroadcastReceiver extends BroadcastReceiver
  {
    private static final int MILLIS_IN_ONE_MINUTE = 60000;
    private static final int MILLIS_IN_ONE_SECOND = 1000;

    @Override
    public void onReceive(Context context, Intent intent)
    {
      long duration        = intent.getLongExtra(EXTRA_DURATION, -1);
      long currentPosition = intent.getLongExtra(EXTRA_CURRENT_POSITION, -1);

      // Save the duration of the current song. We'll need this
      // if the user tries to move the seek bar to convert back
      // to a position within the track.
      mCurrentSongDuration = duration;

      // Much time is left?
      long timeLeft         = duration - currentPosition;
      long remainingMinutes = timeLeft / MILLIS_IN_ONE_MINUTE;
      long remainingSeconds = (timeLeft - (remainingMinutes * MILLIS_IN_ONE_MINUTE)) / MILLIS_IN_ONE_SECOND;

      String timeLeftString = "0:00";
      if (remainingSeconds != 0 || remainingMinutes != 0)
      {
        // Compute time left string.
        timeLeftString = "-" + remainingMinutes + ":" + (remainingSeconds > 9 ? remainingSeconds : "0" + remainingSeconds);
      }

      if (currentPosition >= 0 && duration >= 0)
      {
        mSongTimeText.setText(timeLeftString);
      }

      // Don't update the position of the seek bar while the user is
      // actively moving it.
      if (mUserMovingSeekBar)
      {
        Log.d(TAG, "Skipping seek bar position update. User moving seek bar.");
        return;
      }

      if (duration > -1 && currentPosition > -1)
      {
        int progress = Math.round((currentPosition * SEEK_BAR_STEPS) / duration);

        mSeekBar.setMin(0);
        mSeekBar.setMax(SEEK_BAR_STEPS);
        mSeekBar.setProgress(progress, true);
      }
    }
  }

  @Override
  protected void onNewIntent(Intent intent)
  {
    super.onNewIntent(intent);

    String action = intent.getAction();
    int    state  = intent.getIntExtra(EXTRA_STATE, -1);

    if (ACTION_UPDATE_STATE.equals(action))
    {
      switch (state)
      {
        case STATE_PLAYING:
          setPlayPauseState(true);
          break;
        case STATE_PAUSED:
          setPlayPauseState(false);
          break;
        case STATE_STOPPED:
          setPlayPauseState(false);
          break;
      }

      setAlbumAndSongUi(intent);
    }
  }

  private void setAlbumAndSongUi(Intent intent)
  {
    int    drawableResource = intent.getIntExtra(EXTRA_ALBUM_DRAWABLE_RESOURCE, -1);
    String albumTitle       = intent.getStringExtra(EXTRA_ALBUM_TITLE);
    String songTitle        = intent.getStringExtra(EXTRA_SONG_TITLE);
    String artistName       = intent.getStringExtra(EXTRA_ARTIST_NAME);

    if (drawableResource > -1 && !TextUtils.isEmpty(albumTitle) && !TextUtils.isEmpty(songTitle) && !TextUtils
        .isEmpty(artistName))
    {
      mAlbumCoverImage.setImageDrawable(getDrawable(drawableResource));
      mArtistAndAlbumText.setText(artistName + " - " + albumTitle);
      mSongText.setText(songTitle);
    }
  }

  private void setPlayPauseState(boolean playing)
  {
    if (playing)
    {
      mIsPlaying = true;
      mPlayPauseButton.setBackground(getDrawable(R.drawable.ic_pause_circle_filled_black_24dp));
    }
    else
    {
      mIsPlaying = false;
      mPlayPauseButton.setBackground(getDrawable(R.drawable.ic_play_circle_filled_black_24dp));
    }
  }

  private void setupUi()
  {
    mSkipPreviousButton = findViewById(R.id.skipPreviousButton);
    mPlayPauseButton = findViewById(R.id.playPauseButton);
    mSkipNextButton = findViewById(R.id.skipNextButton);
    mStopButton = findViewById(R.id.stopButton);
    mAlbumCoverImage = findViewById(R.id.albumCoverImage);
    mArtistAndAlbumText = findViewById(R.id.artistAndAlbumText);
    mSongText = findViewById(R.id.songText);
    mSeekBar = findViewById(R.id.seekBar);
    mSongTimeText = findViewById(R.id.songTimeText);

    mSkipPreviousButton.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        Log.d(TAG, "Skip previous");

        Intent skipIntent = new Intent(MainActivity.this, MusicService.class);
        skipIntent.setAction(MusicService.ACTION_SKIP_PREVIOUS);

        MainActivity.this.startService(skipIntent);
      }
    });

    mPlayPauseButton.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        Log.d(TAG, "Play pause button");

        Intent skipIntent = new Intent(MainActivity.this, MusicService.class);

        if (mIsPlaying)
        {
          skipIntent.setAction(MusicService.ACTION_PAUSE);
        }
        else
        {
          skipIntent.setAction(MusicService.ACTION_PLAY);
        }

        MainActivity.this.startService(skipIntent);
      }
    });

    mSkipNextButton.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        Log.d(TAG, "Skip next");
        Intent skipIntent = new Intent(MainActivity.this, MusicService.class);
        skipIntent.setAction(MusicService.ACTION_SKIP_NEXT);

        MainActivity.this.startService(skipIntent);
      }
    });

    mStopButton.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        Log.d(TAG, "Stop");
        Intent stopIntent = new Intent(MainActivity.this, MusicService.class);
        stopIntent.setAction(MusicService.ACTION_STOP);

        MainActivity.this.startService(stopIntent);
      }
    });

    mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
    {

      int mProgress = -1;
      boolean mFromUser = false;

      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
      {
        mProgress = progress;
        mFromUser = fromUser;
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar)
      {
        // Should ignore updates from the music service, until the user has let go
        // of the seek bar so that the user and music service are not competing for
        // it's attention.
        mUserMovingSeekBar = true;
      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar)
      {
        Log.d(TAG, "onStopTrackingTouch(seekBar: " + seekBar + ")");
        Log.d(TAG, "Settled on progress: " + mProgress + ", from user: " + mFromUser);
        long position = (mProgress * mCurrentSongDuration) / SEEK_BAR_STEPS;

        Intent seekToIntent = new Intent(MainActivity.this, MusicService.class);
        seekToIntent.setAction(MusicService.ACTION_SEEK_TO);
        seekToIntent.putExtra(MusicService.EXTRA_SEEK_POSITION, position);

        startService(seekToIntent);
        mUserMovingSeekBar = false;
      }
    });

    Intent pingServiceIntent = new Intent(this, MusicService.class);
    pingServiceIntent.setAction(MusicService.ACTION_STATE_REQUEST);

    startService(pingServiceIntent);
  }
}
