package com.softwaresushi.scottify;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.Timer;
import java.util.TimerTask;

public class MusicService extends Service
{
  public static final String ACTION_SKIP_PREVIOUS = BuildConfig.APPLICATION_ID + ".SKIP_PREVIOUS";
  public static final String ACTION_PLAY          = BuildConfig.APPLICATION_ID + ".PLAY";
  public static final String ACTION_PAUSE         = BuildConfig.APPLICATION_ID + ".PAUSE";
  public static final String ACTION_SKIP_NEXT     = BuildConfig.APPLICATION_ID + ".SKIP_NEXT";
  public static final String ACTION_STOP          = BuildConfig.APPLICATION_ID + ".STOP";
  public static final String ACTION_STATE_REQUEST = BuildConfig.APPLICATION_ID + ".STATE_REQUEST";
  public static final String ACTION_SEEK_TO       = BuildConfig.APPLICATION_ID + ".SEEK_TO";

  public static final String EXTRA_SEEK_POSITION = "position";

  private static final String TAG                        = Constants.TAG + "ms";
  private static final int    FOREGROUND_NOTIFICATION_ID = 73;
  private static final String CHANNEL_MUSIC              = "music";

  SimpleExoPlayer mExoPlayer;

  private long mCurrentPosition;
  private int  mCurrentWindowIndex;

  String[] mSongUriArray = {"https://test.softwaresushi.com/sacred_heart.mp3",
                            "https://test.softwaresushi.com/all_the_fools_sailed_away.mp3",
                            "https://test.softwaresushi.com/dont_talk_to_strangers.mp3"};

  int[] mAlbumCoverArray = {R.drawable.dio_sacred_heart,
                            R.drawable.dream_evil,
                            R.drawable.holy_diver};

  String[] mArtistArray = {"Dio",
                           "Dio",
                           "Dio"};

  String[] mAlbumTitleArray = {"Sacred Heart",
                               "Dream Evil",
                               "Holy Diver"};

  String[] mSongTitleArray = {"Sacred Heart",
                              "All The Fools Sailed Away",
                              "Don't Talk To Strangers"};

  PlayerNotificationManager mPlayerNotificationManager;

  Timer mProgressTimer;

  LocalBroadcastManager mLocalBroadcastManager;

  @Override
  public void onCreate()
  {
    super.onCreate();

    mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {

    Log.d(TAG, "Got new Intent: " + intent);

    if (intent == null) {
      return START_NOT_STICKY;
    }

    String action = intent.getAction();

    if (action != null)
    {

      switch (action)
      {
        case ACTION_PLAY:
          try
          {
            play();
          }
          catch (Exception e)
          {
            Log.e(TAG, "The cake was a lie.");
          }
          break;
        case ACTION_PAUSE:
          pause();
          break;
        case ACTION_STOP:
          stop();
          break;
        case ACTION_SKIP_NEXT:
          skipNext();
          break;
        case ACTION_SKIP_PREVIOUS:
          skipPrevious();
          break;
        case ACTION_SEEK_TO:
          seekTo(intent);
          break;
        case ACTION_STATE_REQUEST:
          getAndSendStateUpdate();
          break;
      }
    }
    return START_STICKY;
  }

  private void seekTo(Intent intent)
  {
    if (mExoPlayer != null && mExoPlayer.getPlayWhenReady())
    {
      if (intent != null)
      {
        long position = intent.getLongExtra(EXTRA_SEEK_POSITION, C.INDEX_UNSET);

        if (position != C.INDEX_UNSET)
        {
          mExoPlayer.seekTo(position);
        }
      }
    }
  }

  private void skipNext()
  {
    if (mExoPlayer != null)
    {
      skip(mExoPlayer.getNextWindowIndex());
    }
  }

  private void skipPrevious()
  {
    if (mExoPlayer != null)
    {
      skip(mExoPlayer.getPreviousWindowIndex());
    }
  }

  private void skip(int windowIndex)
  {
    if (mExoPlayer != null)
    {
      // IF we can skip
      if (windowIndex != C.INDEX_UNSET)
      {
        mCurrentPosition = 0;
        mCurrentWindowIndex = windowIndex;
        mExoPlayer.seekTo(mCurrentWindowIndex, mCurrentPosition);
      }
    }
  }

  private void getAndSendStateUpdate()
  {
    int    state;
    int    albumCoverResource = -1;
    String albumText          = null;
    String songText           = null;
    String artistName         = null;

    if (mExoPlayer == null)
    {
      state = MainActivity.STATE_STOPPED;
    }
    else
    {
      if (mExoPlayer.getPlayWhenReady())
      {
        state = MainActivity.STATE_PLAYING;
      }
      else
      {
        state = MainActivity.STATE_PAUSED;
      }

      int currentWindowIndex = mExoPlayer.getCurrentWindowIndex();

      albumCoverResource = mAlbumCoverArray[currentWindowIndex];
      albumText = mAlbumTitleArray[currentWindowIndex];
      artistName = mArtistArray[currentWindowIndex];
      songText = mSongTitleArray[currentWindowIndex];

    }

    sendStateUpdateIntent(state, albumCoverResource, albumText, songText, artistName);
  }

  private void sendStateUpdateIntent(int state, int albumCoverResource, String albumText, String songText, String
      artistText)
  {
    Intent intent = new Intent(this, MainActivity.class);
    intent.setAction(MainActivity.ACTION_UPDATE_STATE);
    intent.putExtra(MainActivity.EXTRA_STATE, state);

    intent.putExtra(MainActivity.EXTRA_ALBUM_DRAWABLE_RESOURCE, albumCoverResource);
    intent.putExtra(MainActivity.EXTRA_ALBUM_TITLE, albumText);
    intent.putExtra(MainActivity.EXTRA_SONG_TITLE, songText);
    intent.putExtra(MainActivity.EXTRA_ARTIST_NAME, artistText);

    // Need this flag for Android 8 +
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    startActivity(intent);
  }

  private void stop()
  {
    Log.d(TAG, "Stopping");
    if (mExoPlayer != null)
    {
      mCurrentPosition = 0;
      mExoPlayer.stop();
      mExoPlayer.release();
      mExoPlayer = null;
    }

    getAndSendStateUpdate();
    stopForeground(true);
    stopProgressTimer();
    stopSelf();
  }

  @Override
  public void onDestroy()
  {
    if (mPlayerNotificationManager != null)
    {
      mPlayerNotificationManager.setPlayer(null);
    }

    if (mExoPlayer != null)
    {
      mExoPlayer.stop();
      mExoPlayer.release();
      mExoPlayer = null;
    }

    super.onDestroy();
  }

  LoopingMediaSource mMediaSource;

  private void play()
  {

    if (mExoPlayer == null)
    {
      mExoPlayer = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());
      DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this,
                                                                                                        "Scottify"));

      setupExoPlayerEventListener();

      if (mMediaSource == null)
      {
        ConcatenatingMediaSource cms = new ConcatenatingMediaSource();

        for (String uri : mSongUriArray)
        {
          cms.addMediaSource(new ExtractorMediaSource.Factory(dataSourceFactory)
                                 .createMediaSource(Uri.parse(uri)));
        }

        mMediaSource = new LoopingMediaSource(cms);
      }

      mExoPlayer.prepare(mMediaSource);
      mExoPlayer.setPlayWhenReady(true);

      // Start thread that polls for timing info?
      startProgressTimer();

    }
    else
    {
      if (!mExoPlayer.getPlayWhenReady())
      {
        if (mCurrentPosition > 0 || mCurrentWindowIndex > 0)
        { mExoPlayer.seekTo(mCurrentWindowIndex, mCurrentPosition); }
        else
        {
          mExoPlayer.prepare(mMediaSource);
        }
        mExoPlayer.setPlayWhenReady(true);
      }
    }
    createNotification();
    getAndSendStateUpdate();
    Log.d(TAG, "Playing");
  }

  private void startProgressTimer()
  {
    TimerTask tt = new TimerTask()
    {
      @Override
      public void run()
      {
        getAndSendSeekBarUpdateIntent();
      }
    };

    if (mProgressTimer == null)
    {
      mProgressTimer = new Timer();
    }

    mProgressTimer.schedule(tt, 0, 250);
  }

  private void getAndSendSeekBarUpdateIntent()
  {
    if (mExoPlayer != null)
    {
      long duration        = mExoPlayer.getDuration();
      long currentPosition = mExoPlayer.getCurrentPosition();

      sendSeekBarUpdateIntent(duration, currentPosition);
    }
  }

  private void sendSeekBarUpdateIntent(long duration, long currentPosition)
  {
    Intent intent = new Intent(MainActivity.ACTION_UPDATE_SEEK_BAR);
    intent.putExtra(MainActivity.EXTRA_DURATION, duration);
    intent.putExtra(MainActivity.EXTRA_CURRENT_POSITION, currentPosition);

    mLocalBroadcastManager.sendBroadcast(intent);
  }

  private void setupExoPlayerEventListener()
  {
    mExoPlayer.addListener(new Player.EventListener()
    {
      @Override
      public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason)
      {}

      @Override
      public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections)
      {}

      @Override
      public void onLoadingChanged(boolean isLoading)
      {}

      @Override
      public void onPlayerStateChanged(boolean playWhenReady, int playbackState)
      {
        getAndSendStateUpdate();
        if (playWhenReady)
        {
          startProgressTimer();
        }
        else
        {
          stopProgressTimer();
        }
      }

      @Override
      public void onRepeatModeChanged(int repeatMode)
      {}

      @Override
      public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled)
      {}

      @Override
      public void onPlayerError(ExoPlaybackException error)
      {}

      @Override
      public void onPositionDiscontinuity(int reason)
      {
        getAndSendStateUpdate();
      }

      @Override
      public void onPlaybackParametersChanged(PlaybackParameters playbackParameters)
      {}

      @Override
      public void onSeekProcessed()
      {}
    });
  }

  private void stopProgressTimer()
  {
    if (mProgressTimer != null)
    {
      mProgressTimer.cancel();
    }
  }

  private void createNotification()
  {
    NotificationChannel nc = getNotificationChannel();

    NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    nm.createNotificationChannel(nc);

    Intent startActivityIntent = new Intent(this, MainActivity.class);
    startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    final PendingIntent pendingIntent =
        PendingIntent.getActivity(this, 1, startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    mPlayerNotificationManager = PlayerNotificationManager
        .createWithNotificationChannel(this, CHANNEL_MUSIC,
                                       R.string.ms_channel_name,
                                       FOREGROUND_NOTIFICATION_ID,
                                       new PlayerNotificationManager.MediaDescriptionAdapter()
                                       {
                                         @Override
                                         public String getCurrentContentTitle(Player player)
                                         {
                                           int currentWindowIndex = player.getCurrentWindowIndex();
                                           return mArtistArray[currentWindowIndex] + " - " +
                                                  mAlbumTitleArray[currentWindowIndex];
                                         }

                                         @Nullable
                                         @Override
                                         public PendingIntent createCurrentContentIntent(Player player)
                                         {
                                           return pendingIntent;
                                         }

                                         @Nullable
                                         @Override
                                         public String getCurrentContentText(Player player)
                                         {
                                           int currentWindowIndex = player.getCurrentWindowIndex();
                                           return mSongTitleArray[currentWindowIndex];
                                         }

                                         @Nullable
                                         @Override
                                         public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback callback)
                                         {
                                           int currentWindowIndex = player.getCurrentWindowIndex();
                                           return BitmapFactory.decodeResource(getResources(),
                                                                               mAlbumCoverArray[currentWindowIndex]);
                                         }
                                       });

    mPlayerNotificationManager.setNotificationListener(new PlayerNotificationManager.NotificationListener()
    {
      @Override
      public void onNotificationStarted(int notificationId, Notification notification)
      {
        startForeground(notificationId, notification);
      }

      @Override
      public void onNotificationCancelled(int notificationId)
      {
        stopSelf();
      }
    });

    mPlayerNotificationManager.setSmallIcon(R.drawable.ic_stat_s);
    mPlayerNotificationManager.setPlayer(mExoPlayer);
  }

  @NonNull
  private NotificationChannel getNotificationChannel()
  {
    NotificationChannel nc = new NotificationChannel(CHANNEL_MUSIC, CHANNEL_MUSIC, NotificationManager
        .IMPORTANCE_DEFAULT);
    nc.setDescription(CHANNEL_MUSIC);
    nc.setShowBadge(false);
    nc.setBypassDnd(true);
    nc.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
    nc.enableLights(false);
    return nc;
  }

  private void pause()
  {

    if (mExoPlayer != null && mExoPlayer.getPlayWhenReady())
    {
      mExoPlayer.setPlayWhenReady(false);
      mCurrentPosition = mExoPlayer.getCurrentPosition();
      mCurrentWindowIndex = mExoPlayer.getCurrentWindowIndex();
    }

    getAndSendStateUpdate();
    Log.d(TAG, "Pausing");
    createNotification();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent)
  {
    return null;
  }
}
